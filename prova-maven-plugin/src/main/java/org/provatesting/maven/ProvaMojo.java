/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.maven;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.artifact.resolver.filter.ScopeArtifactFilter;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilder;
import org.apache.maven.shared.dependency.graph.DependencyNode;
import org.apache.maven.shared.dependency.graph.traversal.CollectingDependencyNodeVisitor;
import org.provatesting.commandline.CommandLineRunner;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mojo(
    name = "test",
    defaultPhase = LifecyclePhase.TEST,
    requiresDependencyResolution = ResolutionScope.TEST
)
public class ProvaMojo extends AbstractMojo {

  @Parameter(defaultValue = "${project.build.directory}")
  private String projectBuildDir;

  @Parameter(property = "test.scenarioParser", required = true)
  private String scenarioParser;

  @Parameter(property = "test.resultPrinter", required = true)
  private String resultPrinter;

  @Parameter(property = "test.testResource", required = true)
  private String testResource;

  @Parameter(property = "test.resultOutputPath", required = true)
  private String resultOutputPath;

  @Component
  private MavenSession session;

  @Component
  private MavenProject project;

  @Component
  private DependencyGraphBuilder dependencyGraphBuilder;

  @Component
  private RepositorySystem repositorySystem;

  @Parameter(defaultValue = "${localRepository}", readonly = true)
  private ArtifactRepository localRepository;

  //https://stackoverflow.com/questions/636367/executing-a-java-application-in-a-separate-process/723914#723914

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    List<URL> urlList = findAllClassesForClasspath();

    boolean success = runInSeparateProcess(urlList);

    if (!success) {
      throw new MojoFailureException("There are PROVA test failures.");
    }
  }

  private List<URL> findAllClassesForClasspath() throws MojoExecutionException {
    List<URL> urlList = new ArrayList<>();

    urlList.addAll(loadClassesFromDependencies());
    urlList.addAll(loadClassesFromProjectBuildDir());

    getLog().debug("Found the following urls that need to be loaded:\n" + urlList);

    return urlList;
  }

  private List<URL> loadClassesFromDependencies() throws MojoExecutionException {
    getLog().debug("Using Local Repo System: " + repositorySystem);
    getLog().debug("Using Local Repo: " + localRepository);

    Set<Artifact> artifacts = findArtifactsFromDependencies();

    List<URL> urlList = new ArrayList<>();
    try {
      for (Artifact artifact : artifacts) {
        Set<Artifact> loadedForArtifact = loadActualArtifact(artifact);

        if (loadedForArtifact.isEmpty()) {
          getLog().debug(
              "File for Artifact " + artifact.getArtifactId() + " could not be loaded."
          );
        } else {
          for (Artifact loaded : loadedForArtifact) {
            urlList.add(loaded.getFile().toURI().toURL());
          }
        }
      }
    } catch (MalformedURLException e) {
      throw new MojoExecutionException(e.getMessage());
    }

    return urlList;
  }

  private Set<Artifact> findArtifactsFromDependencies() {
    Set<Artifact> artifacts = new HashSet<>();
    try {
      ArtifactFilter artifactFilter = new ScopeArtifactFilter(Artifact.SCOPE_TEST);

      DependencyNode dependencyNode = dependencyGraphBuilder.buildDependencyGraph(
          project,
          artifactFilter
      );

      CollectingDependencyNodeVisitor visitor = new CollectingDependencyNodeVisitor();
      dependencyNode.accept(visitor);
      for (DependencyNode child : visitor.getNodes()) {
        artifacts.add(child.getArtifact());
      }
    } catch (Throwable t) {
      getLog().debug("Error occurred: " + t.getMessage());
    }
    return artifacts;
  }

  private List<URL> loadClassesFromProjectBuildDir() {
    File projectDir = new File(projectBuildDir);

    List<URL> urls = Collections.emptyList();

    try {
      urls = Arrays.asList(
          new File(projectDir.getAbsolutePath() + "/test-classes/").toURI().toURL(),
          new File(projectDir.getAbsolutePath() + "/classes/").toURI().toURL()
      );
    } catch (MalformedURLException e) {
      getLog().debug("Could not create directory references: " + e.getMessage());
    }

    return urls;
  }

  private Set<Artifact> loadActualArtifact(Artifact artifact) {
    ArtifactResolutionRequest resolveRequest = new ArtifactResolutionRequest();
    resolveRequest.setArtifact(artifact);
    resolveRequest.setLocalRepository(localRepository);

    ArtifactResolutionResult result = repositorySystem.resolve(resolveRequest);
    getLog().debug("Resolving artifact " + artifact.getArtifactId()
        + " " + "produced " + result.getArtifacts() + " artifacts,"
        + " and " + result.getMissingArtifacts() + " missing artifacts.");

    return result.getArtifacts();
  }

  private boolean runInSeparateProcess(List<URL> urlList) {
    String classpath = System.getProperty("java.class.path");
    classpath += File.pathSeparator;
    classpath += urlList.stream().map(this::fixWindows).collect(Collectors.joining(File.pathSeparator));

    String className = CommandLineRunner.class.getName();

    getLog().info("Classpath is: " + classpath);

    String javaBin = System.getProperty("java.home")
        + File.separator + "bin"
        + File.separator + "java";

    ProcessBuilder builder = new ProcessBuilder(
        javaBin, "-cp", classpath, className,
        "-s", this.scenarioParser,
        "-r", this.resultPrinter,
        "-t", this.testResource,
        "-o", this.resultOutputPath
    );

    getLog().info("Executing '" + builder.command() + "'");

    try {
      Process process = builder.inheritIO().start();
      process.waitFor();
      return 0 == process.exitValue();
    } catch (InterruptedException | IOException e) {
      getLog().error(e);
      return false;
    }
  }

  private String fixWindows(URL url) {
    String fileName = url.getFile();
    if (fileName != null && fileName.length() >= 3) {
      String start = fileName.substring(0, 3);
      if (start.matches("/[A-Z]:")) {
        return fileName.substring(1);
      }
    }
    return fileName;
  }

}
