package org.provatesting.actuals;

public class ActualColumn {

  private final String key;

  private final String value;

  public ActualColumn(String key, String value) {
    this.key = key;
    this.value = value;
  }

  public String getKey() {
    return key;
  }

  public String getValue() {
    return value;
  }
}
