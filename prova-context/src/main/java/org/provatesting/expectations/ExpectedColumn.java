package org.provatesting.expectations;

import java.util.Objects;

public class ExpectedColumn implements Comparable<ExpectedColumn> {

  private int columnNumber;
  private String key;
  private DataType dataType;
  private String precision;
  private String value;

  /**
   * Represents the description of a single column in terms of the table.
   * You can think of it as the header info for all rows under this header.
   * @param columnNumber - represents the position in the table
   * @param key - represents the values in the rows underneath
   * @param dataType - used to compare the expected values and actual values
   * @param precision - used to avoid rounding errors in comparisons
   * @param value - the value
   */
  public ExpectedColumn(int columnNumber,
                        String key,
                        String dataType,
                        String precision,
                        String value
  ) {

    this.columnNumber = columnNumber;
    this.key = key;
    if (dataType == null || "".equals(dataType)) {
      this.dataType = DataType.Text;
    } else {
      this.dataType = DataType.valueOf(dataType);
    }
    this.precision = precision;
    this.value = value;
  }

  public int getColumnNumber() {
    return columnNumber;
  }

  public String getKey() {
    return key;
  }

  public DataType getDataType() {
    return dataType;
  }

  public String getPrecision() {
    return precision;
  }

  public String getValue() {
    return value;
  }

  @Override
  public int compareTo(ExpectedColumn o) {
    return Integer.compare(columnNumber, o.columnNumber);
  }

  /**
   * Compares an instance of this type with a given object.
   * @param object - the object to compare to
   * @return true, only if the two objects are of the same type and all properties are equal
   */
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || getClass() != object.getClass()) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }

    ExpectedColumn that = (ExpectedColumn) object;
    return columnNumber == that.columnNumber
        && Objects.equals(key, that.key)
        && Objects.equals(dataType, that.dataType)
        && Objects.equals(precision, that.precision)
        && Objects.equals(value, that.value);
  }

  public int hashCode() {
    return Objects.hash(super.hashCode(), columnNumber, key, dataType, precision, value);
  }
}
