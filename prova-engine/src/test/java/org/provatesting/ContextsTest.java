/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class ContextsTest {

  @Test
  public void test() {
    Contexts contexts = new Contexts();

    Object resource = contexts.get("foo");
    assertNotNull(resource);
    assertTrue(resource instanceof ArrayList);

    resource = contexts.get("bar");
    assertNotNull(resource);
    assertTrue(resource instanceof HashMap);

    try {
      contexts.get("baz");
    } catch (Contexts.ResourceNotFound e) {
    }
  }

  public static class Sample1Context implements Context {

    @Override
    public Class get(String resourceName) {
      if (resourceName.equals("foo")) {
        return ArrayList.class;
      } else {
        return null;
      }
    }
  }

  public static class Sample2Context implements Context {

    @Override
    public Class get(String resourceName) {
      if (resourceName.equals("bar")) {
        return HashMap.class;
      }
      return null;
    }
  }
}
