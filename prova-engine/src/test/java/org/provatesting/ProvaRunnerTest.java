/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting;

import org.mockito.Mockito;
import org.provatesting.actuals.ActualColumn;
import org.provatesting.actuals.ActualRow;
import org.provatesting.annotations.MultiRowAction;
import org.provatesting.annotations.MultiRowValidation;
import org.provatesting.annotations.SingleRowAction;
import org.provatesting.annotations.SingleRowValidation;
import org.provatesting.expectations.ExpectedRow;
import org.provatesting.expectations.InitParameter;
import org.provatesting.parsers.ScenarioParser;
import org.provatesting.parsers.xlsx.XlsxParser;
import org.provatesting.printers.HtmlResultPrinter;
import org.provatesting.printers.PrinterEngine;
import org.provatesting.printers.ResultPrinter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;

import static org.testng.Assert.assertEquals;

public class ProvaRunnerTest {
  private static ExpectedRow actionMethodRow;
  private static SortedSet<ExpectedRow> method_CausingExceptionRows;

  private static int initMethodCount;
  private static int validationMethodCount;
  private static int multiRowValidationMethodCount;

  @BeforeTest
  public void setup() {
    actionMethodRow = null;
    method_CausingExceptionRows = null;

    initMethodCount = 0;
    validationMethodCount = 0;
    multiRowValidationMethodCount = 0;
  }

  @Test
  public void testEndToEnd() throws IOException {
    ScenarioParser parser = new XlsxParser();

    TestablePrinterEngine printerEngine = new TestablePrinterEngine();

    String path = new File("").getAbsolutePath() + "/src/test/resources/SampleTest.xlsx";

    File testResource = new File(path);

    ProvaRunner provaRunner = new ProvaRunner(parser, printerEngine, "");
    provaRunner.run(Collections.singletonList(testResource));

    Mockito.verify(printerEngine.writer).write(getExpectedHtml());

    assertEquals(2, initMethodCount);
    assertEquals(0, multiRowValidationMethodCount);
    assertEquals(1, validationMethodCount);
  }

  private String getExpectedHtml() throws IOException {
    String html = "";

    Reader reader = new InputStreamReader(getClass().getResourceAsStream("/SampleOutputMainEndToEnd.html"));
    BufferedReader bufferedReader = new BufferedReader(reader);
    String line = null;
    String lineSeparator = "";
    while ((line = bufferedReader.readLine()) != null) {
      html += lineSeparator + line;
      lineSeparator = "\n";
    }

    return html;
  }

  public static class SampleContext implements Context {
    @Override
    public Class get(String resourceName) {
      if (resourceName.equals("TestResource")) {
        return MyObject.class;
      }
      return null;
    }

    public final static class MyObject {

      public void myInitMethod(SortedSet<InitParameter> initParameters) {
        initMethodCount++;
      }

      @SingleRowAction(init = "myInitMethod")
      public void actionMethod(ExpectedRow expectedRow) {
        actionMethodRow = expectedRow;
      }

      @MultiRowAction(init = "myInitMethod")
      public void method_CausingException(SortedSet<ExpectedRow> expectedRows) {
        method_CausingExceptionRows = expectedRows;
        throw new RuntimeException("An error occured in the step");
      }

      @MultiRowValidation(init = "myInitMethod")
      public Set<ActualRow> validationMethod() {
        validationMethodCount++;

        ActualRow actual1 = new ActualRow();
        actual1.addActualColumn(new ActualColumn("Key_One", "Value_One"));
        actual1.addActualColumn(new ActualColumn("KeyTwo", "ValueTwo"));
        actual1.addActualColumn(new ActualColumn("Key_Three", "Value B"));
        actual1.addActualColumn(new ActualColumn("Key_Foo", "Baz"));

        ActualRow actual2 = new ActualRow();
        actual2.addActualColumn(new ActualColumn("Key_One", "BLA_"));
        actual2.addActualColumn(new ActualColumn("KeyTwo", "blb"));
        actual2.addActualColumn(new ActualColumn("Key_Three", "Random_Value"));
        actual2.addActualColumn(new ActualColumn("Key_Foo", "bat"));

        return new HashSet<>(Arrays.asList(actual1, actual2));
      }

      @SingleRowValidation(init = "myInitMethod")
      public ActualRow multiRowValidationMethod_unused() {
        multiRowValidationMethodCount++;
        return null;
      }
    }
  }

  private class TestablePrinterEngine implements PrinterEngine {
    private Writer writer = Mockito.mock(Writer.class);

    @Override
    public String getName() {
      return "Test";
    }

    @Override
    public ResultPrinter prepareForScenario(String folderName, String fileName, boolean success) {
      return new HtmlResultPrinter(writer);
    }

    @Override
    public void finalizePrinting(String folderName) {
    }
  }
}
