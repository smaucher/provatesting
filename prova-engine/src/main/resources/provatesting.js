$(document).ready(function() {
	$('.header.passed').click(function() {
		rowPassed = $(this).parent().parent().find('.row.passed');
		rowPassedStyle = window.getComputedStyle(rowPassed[0]);
		if(rowPassedStyle.getPropertyValue('font-size') == '0px') {
			rowPassed.css('font-size', 'inherit');
		} else {
			rowPassed.css('font-size', '0px');
		}
	})
});
