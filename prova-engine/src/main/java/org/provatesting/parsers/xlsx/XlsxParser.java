/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.parsers.xlsx;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.provatesting.expectations.InitParameter;
import org.provatesting.parsers.ScenarioParser;
import org.provatesting.scenario.Scenario;
import org.provatesting.scenario.Step;

public class XlsxParser implements ScenarioParser {

  public static final String DEFAULT_PRECISION = "0.01";
  private static final Set<String> newStepIdentifiers =
      new HashSet<>(Arrays.asList("", "resource"));

  @Override
  public String getName() {
    return "XLSX";
  }

  @Override
  public Scenario parse(String scenarioFolder, String scenarioName, InputStream fileResource) {
    String scenarioNameWithoutExtension = FilenameUtils.getBaseName(scenarioName);
    Scenario scenario = new Scenario(scenarioFolder, scenarioNameWithoutExtension);

    try {
      Workbook wb = WorkbookFactory.create(fileResource);
      Sheet sheet = wb.getSheetAt(0); //TODO: could we access it by "name"?

      FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

      RowsInOrder rowsInOrder = new RowsInOrder(sheet);

      List<CellsInOrder> rowsInStep = new ArrayList<>();
      for (Row row : rowsInOrder) {
        CellsInOrder cellsInOrder = new CellsInOrder(row);
        Cell cell = cellsInOrder.iterator().next();
        String label = cell.getStringCellValue().trim().toLowerCase();
        if (newStepIdentifiers.contains(label)) {
          scenario.addStep(buildStep(evaluator, rowsInStep));
          rowsInStep = new ArrayList<>();
        }
        rowsInStep.add(cellsInOrder);
      }
      scenario.addStep(buildStep(evaluator, rowsInStep));
    } catch (IOException | InvalidFormatException e) {
      e.printStackTrace();
    }

    return scenario;
  }

  private Step buildStep(FormulaEvaluator evaluator, List<CellsInOrder> ris) {
    if (ris.isEmpty()) {
      return null;    //TODO: come up with a better way of handling this?
    }

    //TODO: ugh ugly here...can't we pass that in?!!
    int rowNumber = ris.iterator().next().iterator().next().getRow().getRowNum() + 1;

    Iterator<CellsInOrder> rowsInStep = ris.iterator();

    //get resource name
    Iterator<Cell> rowRepresentingResource = rowsInStep.next().iterator();
    rowRepresentingResource.next();
    String resourceName = rowRepresentingResource.next().getStringCellValue();

    //get action name
    Iterator<Cell> rowRepresentingAction = rowsInStep.next().iterator();
    rowRepresentingAction.next();
    String actionName = rowRepresentingAction.next().getStringCellValue();

    Step step = new Step(rowNumber, resourceName, actionName);

    //build everything else?!       //TODO: really ugly?!
    CellsInOrder keys = null;
    CellsInOrder datatype = null;
    CellsInOrder precision = null;
    List<CellsInOrder> values = new ArrayList<>();
    while (rowsInStep.hasNext()) {
      CellsInOrder row = rowsInStep.next();

      String label = row.iterator().next().getStringCellValue().trim().toLowerCase();
      switch (label) {
        case "initparameter":
          step.addInitParam(buildInitParameter(evaluator, row));
          break;
        case "keys":
          keys = row;
          break;
        case "datatype":
          datatype = row;
          break;
        case "precision":
          precision = row;
          break;
        case "values":
          values.add(row);
          break;
        default:
          System.out.println("Unknown Attribute: " + label);
      }
    }

    step.addKeys(keys);

    int valueRowInStep = 1;     //TODO: should this be "absolute", and not "relative"?
    for (CellsInOrder value : values) {
      step.addExpectedRow(
          buildExpectedRow(evaluator, valueRowInStep++, keys, datatype, precision, value)
      );
    }

    return step;
  }

  private ExpectedRow buildExpectedRow(FormulaEvaluator evaluator,
                                       int rowNumber,
                                       CellsInOrder keys,
                                       CellsInOrder datatype,
                                       CellsInOrder precisions,
                                       CellsInOrder values
  ) {

    if (keys == null) {
      throw new RuntimeException("Keys are required!");
    }

    ExpectedRow row = new ExpectedRow(rowNumber);

    //TODO: what if we have "gaps"??
    Iterator<Cell> keysIter = keys.iterator();
    keysIter.next();

    Iterator<Cell> dataTypeIter = datatype.iterator();
    dataTypeIter.next();

    Iterator<Cell> precisionIter = precisions.iterator();
    precisionIter.next();

    Iterator<Cell> valueIter = values.iterator();
    valueIter.next();

    while (keysIter.hasNext()) {
      Cell key = keysIter.next();

      if (key.getStringCellValue() == null || "".equals(key.getStringCellValue().trim())) {
        break;  //TODO: really ugly....
      }

      String dataType = dataTypeIter.hasNext() ? dataTypeIter.next().getStringCellValue() : "Text";
      String precision = precisionIter.hasNext() ? precisionIter.next().toString() : "";
      String value = loadValueFromCell(evaluator, valueIter.next());

      if ("".equals(precision.trim())) {
        if ("number".equalsIgnoreCase(dataType)) {
          precision = DEFAULT_PRECISION;
        }
      }

      row.addExpectedColumn(new ExpectedColumn(
          key.getColumnIndex() + 1,
          key.getStringCellValue(),
          dataType,
          precision,
          value
      ));
    }

    return row;
  }

  private String loadValueFromCell(FormulaEvaluator evaluator, Cell cell) {
    CellValue cellValue = evaluator.evaluate(cell);
    switch (cellValue.getCellTypeEnum()) {
      case BLANK:
        return "";
      case BOOLEAN:
        return Boolean.toString(cell.getBooleanCellValue());
      case NUMERIC:
        return Double.toString(cell.getNumericCellValue());
      case STRING:
        return cell.getStringCellValue();
      case FORMULA:
        //should not happen...was evaluated in the switch statement
        //see https://poi.apache.org/spreadsheet/eval.html
        return "#FORMULA";
      case ERROR:
        return "#ERROR";
      default:
        return "#UNKNOWN";
    }
  }

  private InitParameter buildInitParameter(FormulaEvaluator evaluator, CellsInOrder row) {
    Iterator<Cell> cells = row.iterator();

    //TODO: ugh ugly here...can't we pass that in?!!
    int rowNumber = cells.next().getRow().getRowNum() + 1;
    String paramName = cells.next().getStringCellValue();
    String paramValue = loadValueFromCell(evaluator, cells.next());

    return new InitParameter(rowNumber, paramName, paramValue);
  }

  public abstract static class ItemsInOrder<K, V> implements Iterable<V> {

    private List<V> rows;

    /**
     * Orders the provided iterable in an order based on the key's compare method.
     * @param raw - the raw iterable
     */
    public ItemsInOrder(Iterable<V> raw) {
      Map<K, V> map = new HashMap<>();
      for (V r : raw) {
        map.put(getKey(r), r);
      }

      TreeSet<K> keys = new TreeSet<>(map.keySet());
      rows = new ArrayList<>();
      for (K key : keys) {
        rows.add(map.get(key));
      }
    }

    protected abstract K getKey(V value);

    @Override
    public Iterator<V> iterator() {
      return rows.iterator();
    }
  }

  public static final class RowsInOrder extends ItemsInOrder<Integer, Row> {
    public RowsInOrder(Iterable<Row> rows) {
      super(rows);
    }

    @Override
    protected Integer getKey(Row value) {
      return value.getRowNum();
    }
  }

  public static final class CellsInOrder extends ItemsInOrder<Integer, Cell> {
    public CellsInOrder(Iterable<Cell> cells) {
      super(cells);
    }

    @Override
    protected Integer getKey(Cell value) {
      return value.getColumnIndex();
    }
  }
}
