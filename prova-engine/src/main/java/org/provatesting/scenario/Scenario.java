/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.scenario;

import java.util.SortedSet;
import java.util.TreeSet;

public class Scenario {

  private String folder;
  private String name;
  private SortedSet<Step> steps;

  /**
   * Represents a scenario.
   * @param scenarioFolder - the containing folder (think of it as a directory on the HDD)
   * @param scenarioName - the actual scenario name
   */
  public Scenario(String scenarioFolder, String scenarioName) {
    this.folder = scenarioFolder;
    this.name = scenarioName;
    steps = new TreeSet<>();
  }

  public String getFolderName() {
    return folder;
  }

  public String getName() {
    return name;
  }

  /**
   * Adds a step to the scenario.
   * @param step - if the step is null, we move on with our lives
   */
  public void addStep(Step step) {
    if (step == null) {
      return;
    }
    steps.add(step);
  }

  public SortedSet<Step> getSteps() {
    return steps;
  }

  /**
   * Indicates if all the steps in the scenario are valid.
   * @return true if all, and only if all, steps indicate that they are valid
   */
  public boolean isValid() {
    long validSteps = steps.parallelStream()
        .filter(Step::isValid)
        .count();

    return steps.size() == validSteps;
  }
}
