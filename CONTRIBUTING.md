### Contributors and Contributions are welcome.

Anyone can be a contributor to our project. Being a contributor means that you are interested in improving this project, and contribute in one way or another, ranging from asking questions to providing bugfixes and new features.

Regardless of your role in the project, we ask you to adhere to our [Code of Conduct](CODE_OF_CONDUCT.md).

1) For questions please contact us via email: provatesting.org@gmail.com 
2) For bug fixes, enhancements, file an issue. The more detail the better, even better if you have code ready to commit.
3) For code contributions create a [PR](https://confluence.atlassian.com/bitbucket/pull-requests-and-code-review-223220593.html)

By contributing to our code and/or issuing PRs, you give us permission to use all your contributions under our [license](LICENSE) (MIT License).

Happy (and good) coding!
